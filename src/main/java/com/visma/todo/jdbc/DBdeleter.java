package com.visma.todo.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by rokas.pranskevicius on 2016-07-28.
 */
@Service
public class DBdeleter {

    @Autowired
    private DBconnection connection;

    public void delete(int id){
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            String DELETE_TASK = "DELETE FROM alltasks WHERE id = ?";
            statement = c.prepareStatement(DELETE_TASK);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    private void closeStatement(java.sql.Statement statement) {
        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
