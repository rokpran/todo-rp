package com.visma.todo.jdbc;


import com.visma.todo.tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
public class DBinserter {
    @Autowired
    private DBconnection connection;

    private static final String insert_task = "INSERT INTO alltasks (type, name, starttime,duration,endtime,location) VALUES (?, ?,?,?,?,?)";

        public void insert(Task obj){
            PreparedStatement statement = null;
            try (Connection c = connection.connect()){

                statement = c.prepareStatement(insert_task, Statement.RETURN_GENERATED_KEYS);

                statement.setString(1, obj.getType());
                statement.setString(2, obj.getName());
                statement.setString(3, obj.getStartTime());
                statement.setString(4, obj.getDuration());
                statement.setString(5, obj.getEndTime());
                statement.setString(6, obj.getLocation());
                statement.executeUpdate();

                ResultSet rs = statement.getGeneratedKeys();
                if ( rs.next() ) {
                    // Retrieve the auto generated key(s).
                    int key = rs.getInt(1);
                    obj.setId(key);
                }
                System.out.println(obj.getId());

            } catch (SQLException e) {
                throw new RuntimeException(e);
            } finally {
                closeStatement(statement);
            }
        }

    private void closeStatement(Statement statement) {
        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}