package com.visma.todo.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by rokas.pranskevicius on 2016-08-01.
 */
@Component
@ConfigurationProperties(prefix = "captions")
public class CaptionProperties {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}