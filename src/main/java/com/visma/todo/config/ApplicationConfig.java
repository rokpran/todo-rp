package com.visma.todo.config;

import com.visma.todo.captions.CaptionService;
import com.visma.todo.captions.SimpleCaptionService;
import com.visma.todo.config.properties.CaptionProperties;
import com.visma.todo.config.properties.InputScannerProperties;
import com.visma.todo.data.ConsoleInputScanner;
import com.visma.todo.data.InputScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * Created by rokas.pranskevicius on 2016-08-01.
 */
@Configuration
public class ApplicationConfig {

    @Autowired
    private InputScannerProperties inputScannerProperties;

    @Autowired
    private CaptionProperties captionProperties;

    @Bean
    public InputScanner inputScanner() {
        final InputScannerType type = InputScannerType.valueOf(inputScannerProperties.getType());

        if (Objects.equals(type, InputScannerType.CONSOLE)) {
            return new ConsoleInputScanner();
        }

        return new ConsoleInputScanner();
    }

    @Bean
    public CaptionService captionService() {
        final CaptionType type = CaptionType.valueOf(captionProperties.getType());

        if (Objects.equals(type, CaptionType.SIMPLE)) {
            return new SimpleCaptionService();
        }

        return new SimpleCaptionService();
    }

    private enum InputScannerType {
        CONSOLE
    }

    private enum CaptionType {
        SIMPLE
    }
}