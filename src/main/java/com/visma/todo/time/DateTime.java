package com.visma.todo.time;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by rokas.pranskevicius on 7/20/2016.
 */
@Service
public class DateTime {
    Scanner scanner = new Scanner(System.in);
    String [] time;
    String [] date;
    String days;
    String hours;

    GregorianCalendar gregCal = new GregorianCalendar();

    public String enterTime() {
        hours="";
       // Scanner scanner = new Scanner(System.in);
        hours = scanner.nextLine();
        if (!hours.matches("\\d{2}:\\d{2}")) {
            System.out.println("Wrong format, try again (HH:MM): ");
            enterTime();
        }
        time = hours.split(":");

        if (Integer.parseInt(time[0])>23 || Integer.parseInt(time[0])<00 || Integer.parseInt(time[1])>60 || Integer.parseInt(time[1])<00){
            System.out.println("You entered wrong time, try again: ");
            enterTime();
        }
        System.out.println(hours);
        return hours;
    }

    public String enterDate(){
        days = "";
        date = null;
        days = scanner.nextLine();
        if (!days.matches("\\d{4}-\\d{2}-\\d{2}")) {
            System.out.println("Wrong format, try again (YYYY-MM-DD): ");
            enterDate();
        }
        date = days.split("-");

        if (Integer.parseInt(date[1])>12 || Integer.parseInt(date[1])<1 || Integer.parseInt(date[2])>31 || Integer.parseInt(date[2])<1){
            System.out.println("You have entered wrong month or day, try again: ");
            enterDate();
        }

        //  System.out.println();

        if (Integer.parseInt(date[0])<gregCal.get(GregorianCalendar.YEAR)){
            System.out.println("You have entered past date, try again: ");
            enterDate();
        }
        if (Integer.parseInt(date[0])==gregCal.get(GregorianCalendar.YEAR)){
            if (Integer.parseInt(date[1])>gregCal.get(GregorianCalendar.MONTH)+1){

            }
            if (Integer.parseInt(date[1])<gregCal.get(GregorianCalendar.MONTH)+1){
                System.out.println("You have entered past date, try again: ");
                enterDate();
            }
            if (Integer.parseInt(date[1])==gregCal.get(GregorianCalendar.MONTH)+1){
                if (Integer.parseInt(date[2])>=gregCal.get(GregorianCalendar.DAY_OF_MONTH)){
                }
                if (Integer.parseInt(date[2])<gregCal.get(GregorianCalendar.DAY_OF_MONTH)){
                    System.out.println("You have entered past date, try again: ");
                    enterDate();
                }
            }
        }
        return days;
    }

    public String calcDuration(String meetingStartTime, String durationOfMeeting) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date startTime = df.parse(meetingStartTime);
        String[] splitDurationTime = durationOfMeeting.split(":");
        int durationTime = Integer.parseInt(splitDurationTime[0]) * 60 + Integer.parseInt(splitDurationTime[1]);
        Date endTime = new Date(startTime.getTime() + (durationTime * 60000));
        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String ending = df2.format(endTime);
        System.out.println(ending);
        return ending;
    }

    public static Date parseDate(String a) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date time = df.parse(a);
        return time;
    }

}