package com.visma.todo.controllers;

import com.visma.todo.jdbc.DBarrayReader;
import com.visma.todo.jdbc.DBupdater;
import com.visma.todo.jdbc.DBdeleter;
import com.visma.todo.main.StartMenu;
import com.visma.todo.tasks.Task;
import com.visma.todo.tasks.TaskWebSaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by rokas.pranskevicius on 2016-08-02.
 */
@Service
public class TasksService {
    @Autowired
    TaskWebSaver taskWebSaver;
    @Autowired
    DBarrayReader readingToArray;
    @Autowired
    DBdeleter deleter;
    @Autowired
    DBupdater updater;
    @Autowired
    DBarrayReader reader;

    public List<Task> findAllTasks(String sort) {

        if (sort.equals("id")) {
            StartMenu.taskList.clear();
            reader.readingToArray();
            Collections.sort(StartMenu.getTaskList(), Comparator.comparing(Task::getId));
        }
        else if (sort.equals("name")) {
            StartMenu.taskList.clear();
            reader.readingToArray();
            Collections.sort(StartMenu.getTaskList(), Comparator.comparing(e -> e.getName().toLowerCase()));
        }
        else if (sort.equals("time")) {
            StartMenu.taskList.clear();
            reader.readingToArray();
            Collections.sort(StartMenu.getTaskList(), Comparator.comparing(e -> e.getStartTime().toLowerCase()));
        }
        return StartMenu.taskList;
    }

    public Task saveNewTask(Task task) throws ParseException {
        return taskWebSaver.addTask(task);
    }

    public List<Task> deleteTask(int id) {
        deleter.delete(id);
        for (int i=0;i<StartMenu.taskList.size();i++){
            if (StartMenu.taskList.get(i).getId()==id){
                StartMenu.taskList.remove(i);
            }
        }
        return StartMenu.taskList;
    }

    public void updateTask(Task updatedTask, int id, String type) throws ParseException {
        updater.update(updatedTask, id, type);
    }
}