package com.visma.todo.controllers;

import com.visma.todo.tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

/**
 * Created by rokas.pranskevicius on 2016-08-02.
 */
@Controller
public class TasksController {
    @Autowired
    private TasksService taskService;

    private String id="id";
    private String name="name";
    private String time="time";


    @RequestMapping(value = "/task/viewTasks/{sort}", method = RequestMethod.GET)
    public String viewTasksID(@PathVariable String sort, Model model) {

        if (sort.equals("id")){
            model.addAttribute("allTasks", taskService.findAllTasks(id));
            return "tasks/showTasks";
        }
        else if (sort.equals("name")){
            model.addAttribute("allTasks", taskService.findAllTasks(name));
            return "tasks/showTasks";
        }

        else if (sort.equals("time")){
            model.addAttribute("allTasks", taskService.findAllTasks(time));
            return "tasks/showTasks";
        }
            return "tasks/showTasks";
    }

    @RequestMapping(value = "/task/new", method = RequestMethod.GET)
    public String newTaskPage(Task task){
        return "tasks/addTask";
    }

    @RequestMapping(value = "/task/new", method = RequestMethod.POST)
    public String saveNewTask(Task task) throws ParseException {
        taskService.saveNewTask(task);
        return "tasks/successAdd";
    }

    @RequestMapping(value = "/task/updateDelete", method = RequestMethod.GET)
    public String newTaskPage(Model model){
        model.addAttribute("allTasks", taskService.findAllTasks(id));
        return "tasks/crud";
    }

    @RequestMapping(value = "/task/delete/{id}", method = RequestMethod.GET)
    public String deleteTask(@PathVariable int id, Model model){
        model.addAttribute("allTasks", taskService.deleteTask(id));
        return "tasks/crud";
    }

    @RequestMapping(value = "/task/update/{id}/{type}", method = RequestMethod.GET)
    public String updatePage(@PathVariable int id, @PathVariable String type, Task task, Model model){
        model.addAttribute("taskID", id);
        model.addAttribute("taskType", type);
        return "tasks/updateTask";
    }

    @RequestMapping(value = "/task/update/{id}/{type}", method = RequestMethod.POST)
    public String updateTask(@PathVariable int id, @PathVariable String type, Task task, Model model) throws ParseException {
        System.out.println(id);
        System.out.println(type);
        taskService.updateTask(task,id, type);
        model.addAttribute("allTasks", taskService.findAllTasks("id"));
        return "tasks/crud";
    }
}