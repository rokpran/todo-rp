package com.visma.todo.captions;

import org.springframework.stereotype.Service;

/**
 * Created by rokas.pranskevicius on 2016-07-25.
 */
public interface CaptionService {
    String getCaption(final String key);
}
