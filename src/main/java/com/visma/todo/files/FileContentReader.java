package com.visma.todo.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
public class FileContentReader {
    public void readingFile(){
        int taskNumber = 1;
        try (BufferedReader reader = new BufferedReader(new java.io.FileReader("data.txt"))) {
            String line;
            String[] lineArray;
            while ((line = reader.readLine()) != null) {
                lineArray = line.split(";");
                System.out.println(taskNumber + ".   " + lineArray[1]);
                taskNumber++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
