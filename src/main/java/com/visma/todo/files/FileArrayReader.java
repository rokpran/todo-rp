package com.visma.todo.files;

import com.visma.todo.tasks.Meeting;
import com.visma.todo.tasks.Reminder;
import com.visma.todo.tasks.ReserveTime;

import com.visma.todo.main.StartMenu;
import com.visma.todo.tasks.Task;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class FileArrayReader implements Reader {

    private static ArrayList<Task> taskList = StartMenu.getTaskList();

    @Override
    public void readingToArray() {
        try (BufferedReader reader = new BufferedReader(new FileReader("data.txt"))) {
            String line;
            String[] lineArray;
            String whatType;
            while ((line = reader.readLine()) != null) {
                lineArray = line.split(";");
                whatType = lineArray[0];
                switch (whatType) {
                    case "reminder":
                        Reminder remind = new Reminder(lineArray[0],lineArray[1], lineArray[2], lineArray[3], lineArray[4]);
                        taskList.add(remind);

                    case "meeting":
                        Meeting meet = new Meeting(lineArray[0],lineArray[1], lineArray[2], lineArray[3], lineArray[4], lineArray[5]);
                        taskList.add(meet);

                    case "reservation":
                        ReserveTime reserve = new ReserveTime(lineArray[0],lineArray[1], lineArray[2], lineArray[3], lineArray[4]);
                        taskList.add(reserve);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
