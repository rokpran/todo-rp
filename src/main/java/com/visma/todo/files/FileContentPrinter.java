package com.visma.todo.files;

import org.springframework.stereotype.Service;

import java.io.*;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Service
public class FileContentPrinter implements Printer {
    @Override
    public void printingToFile(int whichToDelete) throws IOException { // PRINTS FROM ONE FILE TO ANOTHER WITHOUT PRINTING SPECIFIC LINE, THEN RENAMES NEW FILE
        File inputFile = new File("data.txt");// Your file
        File tempFile = new File("data2.txt");// temp file

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        PrintWriter writer = new PrintWriter(new FileOutputStream(tempFile, true));
        String str;
        int i = 0;
        while ((str = reader.readLine()) != null) {
            if (i != whichToDelete) {
                writer.append(str);
                writer.println();
            }
            i++;
        }

        writer.close();
        reader.close();
        inputFile.delete();
        tempFile.renameTo(inputFile);
    }
}
