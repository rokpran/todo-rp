package com.visma.todo.main;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.data.*;
import com.visma.todo.files.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.visma.todo.tasks.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class StartMenu implements CommandLineRunner{

    @Autowired
    private InputScanner inputScanner;// = new ConsoleInputScanner();
    @Autowired
    private TypeMenu choose;
    @Autowired
    private ShowMenu show;
    @Autowired
    private CrudMenu obj;
    @Autowired
    private PrintCaption captionPrinter;
    @Autowired
    @Qualifier("fileArrayReader")
    private Reader readerToArray;
    @Autowired
    @Qualifier("DBarrayReader")
    private Reader readerToArr;

    private static ArrayList<Reminder> remindersList = new ArrayList<>();
    private static ArrayList<Meeting> meetingsList = new ArrayList<>();
    private static ArrayList<ReserveTime> reservesList = new ArrayList<>();

    public static ArrayList<Task> taskList = new ArrayList<>();

    public void whatToDo() throws IOException, ParseException {
        if (remindersList.isEmpty() && meetingsList.isEmpty() && reservesList.isEmpty() && taskList.isEmpty()) {
            readerToArr.readingToArray();
        }

        captionPrinter.printCaption("menu.choices");
        captionPrinter.printCaption("menu.input");

        final String choice = inputScanner.getInput();

        switch (choice) {
            case "1":
                choose.whichType();
                break;

            case "2":
                show.showTasks();
                break;

            case "3":
                obj.updateOrDelete();
                break;

            case "0":
                exitProgram();
                break;
            default:
                System.exit(0);
        }
    }

    public static ArrayList<Reminder> getRemindersList() {
        return remindersList;
    }

    public static ArrayList<Meeting> getMeetingsList() {
        return meetingsList;
    }

    public static ArrayList<ReserveTime> getReservesList() {
        return reservesList;
    }

    public static ArrayList<Task> getTaskList() {
        return taskList;
    }


    public void exitProgram() {
        System.exit(0);
    }

    @Override
    public void run(String... args) throws Exception {
        whatToDo();
    }
}