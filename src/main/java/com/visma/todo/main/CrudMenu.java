package com.visma.todo.main;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.tasks.Deleter;
import com.visma.todo.data.InputScanner;
import com.visma.todo.tasks.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class CrudMenu {

    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private Deleter deleter;
    @Autowired
    private Updater updater;
    @Autowired
    private PrintCaption captionPrinter;
    @Autowired
    StartMenu theStart;

    public void updateOrDelete() throws IOException, ParseException {

        captionPrinter.printCaption("menu.deleteUpdate");
        String choice = inputScanner.getInput();
        switch (choice) {
            case "1":
                updater.updateTask();
                break;
            case "2":
                deleter.deleteTasks();
                break;
            case "0":
                theStart.whatToDo();
                break;
            default:
                System.exit(0);
        }
    }
}