package com.visma.todo.main;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.tasks.MeetingAdder;
import com.visma.todo.tasks.ReminderAdder;
import com.visma.todo.tasks.ReserveTimeAdder;
import com.visma.todo.data.InputScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Service
public class TypeMenu {

    @Autowired
    private InputScanner inputScanner;// = new ConsoleInputScanner();
    @Autowired
    private ReminderAdder addRemi;
    @Autowired
    private MeetingAdder addMeet;
    @Autowired
    private ReserveTimeAdder addRese;
    @Autowired
    private StartMenu obj;
    @Autowired
    private PrintCaption captionPrinter;

    public void whichType() throws IOException, ParseException {

        captionPrinter.printCaption("menu.addTasks");
        captionPrinter.printCaption("menu.input");

        final String choice = inputScanner.getInput();

        switch (choice) {
            case "1":
                addRemi.aReminder();
                break;

            case "2":
                addMeet.aMeeting();
                break;

            case "3":
                addRese.addReserveTime();
                break;

            case "0":
                obj.whatToDo();
                break;
            default:
                System.exit(0);
        }
    }
}