package com.visma.todo.main;

import com.visma.todo.captions.CaptionService;
import com.visma.todo.captions.PrintCaption;
import com.visma.todo.jdbc.DBselector;
import com.visma.todo.tasks.Deleter;
import com.visma.todo.data.InputScanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Service
public class ShowMenu {
    @Autowired
    private StartMenu obj;
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private Deleter deleter;
    @Autowired
    private DBselector selecting;
    @Autowired
    private PrintCaption captionPrinter;

    private String showAll = "all";
    private String sortID = "id";
    private String sortNAME="name";
    private String sortTIME= "time";
    private String showReminders = "reminder";
    private String showMeetings = "meeting";
    private String showReservations = "reservation";

    public void showTasks() throws IOException, ParseException {

        System.out.println(captionService.getCaption("menu.printTasks"));
        String whichType = inputScanner.getInput();
        switch (whichType) {
            case "1":
                selecting.select(showAll,sortID);
                break;
            case "2":
                selecting.select(showAll,sortNAME);
                break;
            case "3":
                selecting.select(showAll,sortTIME);
                break;
            case "4":
                selecting.select(showReminders, sortID);
                break;
            case "5":
                selecting.select(showMeetings, sortID);
                break;
            case "6":
                selecting.select(showReservations, sortID);
                break;
            case "0":
                obj.whatToDo();
                break;
            default:
                System.exit(0);
        }

        captionPrinter.printCaption("menu.whatNext");
        String whatNext = inputScanner.getInput();

        switch (whatNext) {
            case "1":
                deleter.deleteTasks();
            case "2":
                showTasks();
            case "0":
                System.exit(0);
        }
    }
}
