package com.visma.todo;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigurationProperties
public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .sources(Main.class)
                .run(args);
    }
}