package com.visma.todo.tasks;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.data.InputScanner;
import com.visma.todo.files.Printer;
import com.visma.todo.jdbc.DBdeleter;
import com.visma.todo.jdbc.DBselector;
import com.visma.todo.main.CrudMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Service
public class Deleter {

    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private Printer printerToFile;
    @Autowired
    private DBdeleter deleter;
    @Autowired
    private DBselector selecting;
    @Autowired
    private PrintCaption captionPrinter;
    @Autowired
    private CrudMenu obj;

    private String showAll = "all";
    private String sortID = "id";

    public void deleteTasks() throws IOException, ParseException {

        captionPrinter.printCaption("menu.whichToDeleteFromDB");
        selecting.select(showAll, sortID);
        captionPrinter.printCaption("menu.goBack");
        Scanner scanner = new Scanner(System.in);
        int whichToDelete = Integer.parseInt(scanner.nextLine()) - 1;

        if (whichToDelete + 1 == 0) {
            CrudMenu obj = new CrudMenu();
            obj.updateOrDelete();
        }

        if (whichToDelete +1 < 0) {
            captionPrinter.printCaption("menu.noSuchTask");
            deleteTasks();
        }

        deleter.delete(whichToDelete);

        //READ and WRITE to temp:
        printerToFile.printingToFile(whichToDelete);

        captionPrinter.printCaption("menu.whatNext");
        String whatNext = inputScanner.getInput();

        switch (whatNext) {
            case "1":
                deleteTasks();
                break;
            case "2":
                obj.updateOrDelete();
                break;
            case "0":
                System.exit(0);
        }
    }
}
