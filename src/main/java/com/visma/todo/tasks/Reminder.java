package com.visma.todo.tasks;

/**
 * Created by rokas.pranskevicius on 7/19/2016.
 */
public class Reminder extends Task {

   public Reminder(String type, String name, String startDate, String duration, String endDate){
       super(type,name,startDate,duration,endDate);
   }
}