package com.visma.todo.tasks;

import com.visma.todo.jdbc.DBinserter;
import com.visma.todo.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

import static com.visma.todo.main.StartMenu.taskList;

/**
 * Created by rokas.pranskevicius on 2016-08-04.
 */
@Service
public class TaskWebSaver {
    @Autowired
    private DBinserter insertingToDB;
    @Autowired
    DateTime enteringDateTime;
    public Task addTask(Task task) throws ParseException {
        if (task != null) {

            task.setEndTime(enteringDateTime.calcDuration(task.getStartTime(), task.getDuration()));

            if (task.getType().equals("reminder") || task.getType().equals("reservation")){
                task.setLocation(null);
                insertingToDB.insert(task);
                taskList.add(new Task(task.getType(),task.getId(),task.getName(), task.getStartTime(), task.getDuration(), task.getEndTime()));
            }
            else if (task.getType().equals("meeting")){
                insertingToDB.insert(task);
                taskList.add(new Task(task.getType(),task.getId(),task.getName(), task.getStartTime(), task.getDuration(), task.getEndTime(), task.getLocation()));
            }
        } else {
            System.out.println("TASK IS NULL");
        }
        return task;
    }
}
