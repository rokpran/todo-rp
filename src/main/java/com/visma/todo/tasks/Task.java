package com.visma.todo.tasks;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by rokas.pranskevicius on 2016-07-28.
 */

public class Task {

    private String type;
    private String name;
    private String startTime;
    private String duration;
    private String endTime;
    private String location;
    private int id;

    public Task(){} // default contructor

    Task(String type, String name, String startTime, String duration, String endTime, String location){
        this.type = type;
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.endTime = endTime;
        this.location = location;
    }

    Task(String type, String name, String startTime, String duration, String endTime){
        this.type = type;
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.endTime = endTime;
    }

    public Task(String type, int id, String name, String startTime, String duration, String endTime){
        this.type = type;
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.endTime = endTime;
    }

    public Task(String type, int id, String name, String startTime, String duration, String endTime, String location){
        this.type = type;
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.endTime = endTime;
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getLocation() {
        return location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
